module.exports = function(config) {
  return function actualFunctionality(someArgument){
    return doSomethingWith(config).and(someArgument);
  };
}