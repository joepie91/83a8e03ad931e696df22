module.exports = function(config) {
  return {
    functionalityOne: function(someArgument) {
      return doSomethingWith(config).and(someArgument);
    },
    functionalityTwo: function(someArgument) {
      return doSomethingElseWith(config).and(someArgument);
    }
  };
}