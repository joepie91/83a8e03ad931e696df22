var config = loadConfiguration("config.js");

var someModule = require("./some-module")(config);
var someObjectModule = require("./some-object-module")(config);

someModule("cake");
someObjectModule.functionalityOne("cake");